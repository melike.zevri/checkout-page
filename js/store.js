//get checkboxes for the selected store layout
const checkbox = document.querySelectorAll('.layout-check');

//get the store container
const storeContainer = document.querySelector('.store-container');

const container = document.querySelector('.store .container');

//create the product container that will have a layout
const layout = document.createElement('div');

//define variables for pagination
let currentPage = 1;
let productsPerPage;

//function to get the selected layout and then set it
function getLayout() {
  if(checkbox.length > 0) {
    checkbox.forEach(box => {
      if(box.checked) {
        storeLayout = box.value;
        storeContainer.innerHTML = '';
        layout.className = `layout-${storeLayout}`;
        storeContainer.appendChild(layout);
        container.className = ``;
        container.classList.add(`container`);
        container.classList.add(`${storeLayout}`);
        //if the layout is grid, then there will be 9 products per page
        if (storeLayout == "grid") {
          productsPerPage = 9;
          displayStore(0, productsPerPage);
          updatePagination(productsPerPage);
        //if the layout is list, then there will be 6 products per page
        } else if (storeLayout == "list") {
          productsPerPage = 6;
          displayStore(0, productsPerPage);
          updatePagination(productsPerPage);
        } 
      }
    })
  }
}

//function to display the products
function displayStore(start, end) {
  layout.innerHTML= '';

  //display only the number of products per page
  for (let i = start; i < end && i < productList.length; i++) {
    const product = productList[i];
  
    const productDiv = document.createElement('div'); 
    productDiv.classList.add('product-container');

    //create product template
    productDiv.innerHTML = `
      <div class="product-card">
        ${product.discount !== undefined ? `<div class="discount-bubble">-${product.discount * 100}%</div>` : ``}
        <div class="product-img">
          <img src="https://static.thenounproject.com/png/3674277-200.png">
        </div>
        <div class= "product-info">
          <div class="product-rating"></div>
          <div class="product-name">${product.name}</div>
          <div class="product-details">${product.brand}</div>
          ${product.discount !== undefined ? `<div class="product-price"><span class="p-former-price">$${product.price}</span> <span class="p-price">$${product.finalPrice}</span></div>` : `<div class="product-price"><span class="p-price">$${product.finalPrice}</span></div>`}
          <div class="product-description">${product.description}</div>
          <button class="product-add add-cart" id="${product.id}">+ ADD TO CART</button>
        </div>
      </div>`;
    
    //create stars based on product.rating
    const ratingDiv = productDiv.querySelector('.product-rating');

    for (let i = 0; i < product.rating; i++) {
      const star = document.createElement('i');
      star.classList.add('bi','bi-star','filled');
      ratingDiv.appendChild(star);
    }

    for (let i = product.rating; i < 5; i++) {
      const star = document.createElement('i');
      star.classList.add('bi', 'bi-star', 'unfilled');
      ratingDiv.appendChild(star);
    }

    //append products to the layout
    layout.appendChild(productDiv);
  };
}

//function to handle pagination
function updatePagination(productsPerPage) {
  //get pagination container
  const pagination = document.querySelector('.pagination');

  //calculate no. of pages
  const totalPages = Math.ceil(productList.length / productsPerPage);

  pagination.innerHTML = '';

  //create pagination buttons
  if (currentPage > 1) {
    const pageButton = document.createElement('button');
    pageButton.innerHTML = '<i class="bi bi-arrow-left"></i> Previous';

    pagination.appendChild(pageButton);
  
    pageButton.addEventListener('click', () => {
      currentPage -=1;
      displayStore((currentPage - 1) * productsPerPage, currentPage * productsPerPage);
      updatePagination(productsPerPage);
    })
  } 

  const pageNr = document.createElement('div');
  pageNr.innerHTML = `${currentPage} / ${totalPages}`;
  pagination.appendChild(pageNr);

  if (currentPage < totalPages) {
    const pageButton = document.createElement('button');
    pageButton.innerHTML = 'Next <i class="bi bi-arrow-right"></i>';

    pagination.appendChild(pageButton);

    pageButton.addEventListener('click', () => {
      currentPage +=1;
      displayStore((currentPage - 1) * productsPerPage, currentPage * productsPerPage);
      updatePagination(productsPerPage);
    })
  }
}

//get the layout when page loads
getLayout();

//get the layout when the boxes are checked
if(checkbox.length > 0) {
  checkbox.forEach(box => {
    box.addEventListener('click', () => {getLayout()})
  })
}

//get all filters
const productFilters = document.querySelectorAll('.checkbox-filter, .price-sorting, .input-price');

//alter display and pagination based on filtering
if(productFilters.length > 0) {
  productFilters.forEach(filter => {
    filter.addEventListener('change', () => {
      displayStore(0, productsPerPage)
      updatePagination(productsPerPage)});
  })
}

