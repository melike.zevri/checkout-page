//get input color containers
const inputColor = document.querySelectorAll('.checkbox-color');

//get input rating containers
const inputRating = document.querySelectorAll('.checkbox-rating');

//style inputs with their value color
if(inputColor.length > 0) {
inputColor.forEach(input => {
    input.style.backgroundColor = `${input.dataset.color}`
})
}

//style inputs with their value rating
if(inputRating.length > 0) {
  inputRating.forEach(input => {
    for (let i = 0; i <  input.dataset.rating; i++) {
      const star = document.createElement('i');
      star.classList.add('bi','bi-star','filled');
      input.appendChild(star);
    }
    for (let i =  input.dataset.rating; i < 5; i++) {
      const star = document.createElement('i');
      star.classList.add('bi', 'bi-star', 'unfilled');
      input.appendChild(star);
    }
  })
}

//function to display the value of the range input 
function displayPrice() {
  const displayValue = document.querySelector('.display-value');
  if(displayValue) {
    displayValue.innerHTML = `Range: $${price.value}`;
    //stylize progress input
    price.style.background= `linear-gradient(to right, #5c9ccc ${price.value}%, #dfeffc ${price.value}%)`
  }
}

//call function when page loads and price input changes
displayPrice();
if(price) {
  price.addEventListener('change', () => {displayPrice()});
  price.addEventListener('mousemove', () => {displayPrice()});
}

document.querySelector('.show-filter-bar').addEventListener('click', () => {
  var menu = document.querySelector('.side-bar');
  menu.classList.add('show');
});

document.querySelector('.hide-filter-bar').addEventListener('click', () => {
  var menu = document.querySelector('.side-bar');
  menu.classList.remove('show');
});