//get cart items container
const cartItemsList = document.querySelectorAll('.cart-items');

//function to display the products in the cart
function displayCart() {
  //get cart from local storage
  const cart = JSON.parse(localStorage.getItem('cart')) || [];

  if(cartItemsList.length > 0) {
    cartItemsList.forEach(cartItems => {
      //create cart items
      cartItems.innerHTML = cart.map(item => `
        <div>
          <div class="product-container">
            <button class="remove-product">x</button>
            <div class="product-img">
              <img src="https://static.thenounproject.com/png/3674277-200.png">
            </div>
            <div class="product-info">
              <p class="product-name">${item.quantity}x ${item.name}</p>
              <div>
                <span class="product-details">${item.size}, ${item.color}</span> 
                <span class="product-price">$${item.quantity * item.finalPrice}</span>
              </div>
            </div>  
          </div>
        </div>`).join('');}) 
  }

  //get remove from cart buttons
  const removeBtn = document.querySelectorAll('.remove-product');

  //add functionality to remove buttons
  removeBtn.forEach((remove, index) => {
    remove.addEventListener('click', () => {
      cart.splice(index, 1);
      localStorage.setItem('cart', JSON.stringify(cart));
      displayCart();
    });
  });   
}

//function to create add to cart buttons node list
function setBtns() {
  const addBtn = document.querySelectorAll('.product-add');
  if(addBtn.length > 0) {
    addBtn.forEach(btn => {    
      btn.addEventListener('click', () => {
        displayCart();})
    }) 
  }
}

//update button node list when the store is filtered
if(productFilters.length > 0) {
  productFilters.forEach(filter => {
    filter.addEventListener('change', () => {
      setBtns();
      displayCart();})
  })
}

//update button node list when the layout changes
if(checkbox.length > 0) {
  checkbox.forEach(box => {
    box.addEventListener('change', () => {
      setBtns();
      displayCart();});
  });
}

//call functions when page loads
setBtns();
displayCart();


