//get the checkboxes that filter products
const checkboxFilters = document.querySelectorAll('.checkbox-filter');

//set up the array of products that should be displayed
let productList = [];

//function to update the product list array
function updateProducts(array) {
    productList = array;
}

//function to handle filtering the products based on the checked boxes
function filterStore() {
  //find boxes that are checked
  const checkedBoxes = Array.from(checkboxFilters).filter(box => box.checked);

  if (checkedBoxes.length > 0) { 
    const filteredProducts = products.filter(product => checkedBoxes.some(box => product[box.name] === box.value));
    updateProducts(filteredProducts);
  } else {
    //if no boxes checked, the product list is the same as the products array
    updateProducts(products);
  }
}
  
//function to order products based on price
function priceFilter(direction) {
  //order products from lowest to highest
  if(direction == "low"){
    const sortedProducts = products.slice().sort((a, b) => a.finalPrice - b.finalPrice);
    updateProducts(sortedProducts);
  //order products from highest to lowest
  } else if (direction == "high") {
    const sortedProducts = products.slice().sort((a, b) => b.finalPrice - a.finalPrice);
    updateProducts(sortedProducts);
  //display products normally
  } else {
    updateProducts(products);
  }
}

//get price order input
const priceSelect = document.querySelector('.price-sorting');

//filter products based on price range
if(priceSelect) {
  priceSelect.addEventListener('change', () => {
    priceFilter(priceSelect.options[priceSelect.selectedIndex].value);
  })
}

//listen for box checking
if(checkboxFilters.length > 0) {
  checkboxFilters.forEach(box => {
    box.addEventListener('change', () => {
      filterStore()});
  })
}
  
//get price range input
const price = document.querySelector('.input-price');

//listen for price range change event
if(price) {
  price.addEventListener('change', () => {
    const filteredArray = products.filter(product => product.price < price.value)
    updateProducts(filteredArray);
  });
}

//set up product list with all products when page loads
updateProducts(products);