//get checkout container
const shoppingList = document.querySelector('.shopping-list');

//function to display checkout items
function displayCheckout() {
  //call cart from local storage
  const cart = JSON.parse(localStorage.getItem('cart')) || [];

  //create checkout items template
  if(shoppingList) {
    shoppingList.innerHTML = cart.map(item => `
      <div>
        <div class= "product-container">
          <button class="remove-checkout">
            <span>x</span>
          </button>
          <div class="product-img">
            <img src="https://static.thenounproject.com/png/3674277-200.png">
          </div>
          <div class="product-info">
            <p class="product-name">${item.name}</p>
            <span class="product-details">${item.size}, ${item.color}</span>
          </div>
          <div class="product-price">
            ${item.discount !== undefined ? `<span class="p-former-price">$${item.price}</span> <span class="p-price">$${item.finalPrice}</span>` : `<span class="p-price">$${item.finalPrice}</span>`}
          </div>
          <div class="product-quantity">
            <button class="minus">-</button>
            <div>${item.quantity}</div>
            <button class="plus">+</button>
          </div>
          <div class="product-total-price">
            <span>$${(item.quantity * item.finalPrice).toFixed(2)}</span>
          </div>
        </div>
      </div>`).join('');
  }

  //button to increase item quantity
  const plusBtn = document.querySelectorAll('.plus');

  //add functionality to button
  if(plusBtn.length > 0) {
    plusBtn.forEach((plus, index) => {
      plus.addEventListener('click', () => {
        cart[index].quantity++;
        localStorage.setItem('cart', JSON.stringify(cart));
        displayCheckout();
      });
    });
  }

  //button to decrease item quantity
  const minusBtn = document.querySelectorAll('.minus');

  //add functionality to button
  if(minusBtn.length > 0) {
    minusBtn.forEach((minus, index) => {
      minus.addEventListener('click', () => {
        if(cart[index].quantity > 1) {
          cart[index].quantity--;
        } else {
          cart.splice(index, 1);
        }
        localStorage.setItem('cart', JSON.stringify(cart));
        displayCheckout();
      });
    });
  }

  //button to remove items from checkout
  const removeBtn = document.querySelectorAll('.remove-checkout');
  
  //add functionality to button
  if(removeBtn.length > 0) {
    removeBtn.forEach((remove, index) => {
      remove.addEventListener('click', () => {
        cart.splice(index, 1);
        localStorage.setItem('cart', JSON.stringify(cart));
        displayCheckout();
      });
    });
  }
}

//display checkout when page loads
displayCheckout();


