//get cart quantity bubbles
const cartQuantity = document.querySelectorAll('.cart-quantity');

//get cart summary containers
const cartSummary = document.querySelectorAll('.cart-summary');
const checkoutSummary = document.querySelector('.checkout-summary');

function updateTotal() {
	//get cart from local storage
  const cart = JSON.parse(localStorage.getItem('cart')) || [];

	//calculate subtotal of products in cart
  const subtotal = cart.reduce((acc, item) => acc + (item.finalPrice * item.quantity), 0);

	//calculate subtotal of undiscounted prices
  const formerSubtotal = cart.reduce((acc, item) => acc + (item.price * item.quantity), 0);

	//declare shipping price
	let shipping = 0; 

	//set shipping price based on subtotal
	if (subtotal < 50 && subtotal > 0) {
  	shipping = 15;
	} else if (subtotal < 150 && subtotal > 0) {
  	shipping = 10;
	}			

	//calculate total final price
	let total = (subtotal + shipping).toFixed(2);

	//calculate number of all items in cart
	const items = cart.reduce((acc, item) => acc + item.quantity, 0);

	//display cart summary in container
	if(cartSummary.length > 0) {
		cartSummary.forEach(summary => {
			summary.innerHTML= `
				<div>
					<p>Shipping</p>
					<p>$${shipping}.00</p>
				</div>
				<div>
					<p>TOTAL</p>
					<p>$${total}</p>
				</div>`;
		})
	}

	//display no. of items in bubble
	if(cartQuantity.length > 0) {
		cartQuantity.forEach(totalItems => {
			//if there are no items in cart, the bubble is not displayed
			if(items != 0) {
				totalItems.style.display = 'flex';
			} else {
				totalItems.style.display = 'none';
			}
			totalItems.textContent = `${items}`;
		})
	}

	//create checkout summary template
	if(checkoutSummary) {
		checkoutSummary.innerHTML = `
			<div class="row">
				<p>SUBTOTAL</p>
					<div>
						<span>$${formerSubtotal.toFixed(2)}</span>
						<span>$${subtotal.toFixed(2)}</span>
					</div>
			</div>
			<div class="row">
				<p>SHIPPING</p>
				<span>$${shipping}.00</span>
			</div>
			<div class="row">
				<p>TOTAL</p>
				<span>$${total}</span>
			</div>`
	}

	//update local storage
	localStorage.setItem('cart', JSON.stringify(cart));
}

//calculate cart summary when buttons are clicked and when page loads
document.addEventListener('click', () => { updateTotal() })
updateTotal();
