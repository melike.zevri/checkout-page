//get all navbar containers
const navbars = document.querySelectorAll('.navbar');

//create the same navbar template on all pages
if(navbars.length > 0){
  navbars.forEach(navbar => {
    navbar.innerHTML= `
      <a href="" class="restart">
        <img src="https://htmldemo.net/oxelar-v1/oxelar/assets/images/logo.webp">
      </a>
      <ul>
        <li><a href="home.html">HOME<a></li>
        <li><a href="">CLOTHING</a></li>
        <li><a href="">JEANS</a></li>
        <li><a href="">SHOP</a></li>
        <li><a href="">BLOG</a></li>
        <li><a href="">PAGES</a></li>
      </ul>
      <ul>
        <li>
          <i class="bi bi-search"></i>
        </li>
        <li>
          <i class="bi bi-cart"></i>
          <div class="cart-container">
            <div class="cart-items"></div>
            <div class="cart-summary"></div>    
            <a href="checkout.html">
              <button>
                  checkout <i class="bi bi-arrow-right"></i>
              </button>
            </a>
          </div>
          </div>
        </li>
        <li class="cart-quantity"></li>
      </ul>`
  })
}