//function to add products to cart
function addToCart() {
  //get all add to cart buttons
  const addBtns = document.querySelectorAll('.product-add');

  if(addBtns.length > 0) {
    addBtns.forEach(btn => {
      btn.addEventListener('click', (event) => {
        //store the cart array in the local storage so the products remain when switching pages
        const cart = JSON.parse(localStorage.getItem('cart')) || [];

        //get the id of the button
        const productId = event.target.id;

        //find the product with the same id
        const selectedProduct = products.find(product => product.id === productId);
        
        //find if the cart already has the product
        const existingItem = cart.find(item => item.id === selectedProduct.id);

        //if the product exists in the cart, increase quantity
        if(existingItem) {
          existingItem.quantity++;
        //if the product doesn't exist, add it to cart
        } else {
          cart.push({ ...selectedProduct, quantity: 1 });
        }
        //update the local storage
        localStorage.setItem('cart', JSON.stringify(cart));  
      })
    })
  }
}

//update button node list when the products are filtered
if (productFilters.length > 0) {
  productFilters.forEach(filter => {
    filter.addEventListener('change', () => {
      addToCart();});
  });
}

//update button node list when the layout changes
if (checkbox.length > 0) {
checkbox.forEach(box => {
  box.addEventListener('change', () => {
    addToCart();
  });
});
}

const paginationBtn = document.querySelectorAll('.pagination button');
if (paginationBtn.length > 0) {
  paginationBtn.forEach(btn => {
    btn.addEventListener('click', () => {
      addToCart();
    });
  });
  }

//create button node list when the page loads
addToCart();

//create the restart button
const restart= document.querySelector('.restart');

//restart button clears the local storage and refreshes page
restart.addEventListener('click', () => {
  localStorage.clear();
});
 
